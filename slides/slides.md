---
title: Writing good tests in Haskell
subtitle: With predicate-transformers
author: Edmund Noble
date: 2019 onwards
theme: Singapore
colortheme: dolphin
fontfamily: noto-sans
header-includes:
- \usepackage{cmbright}
fontsize: 10pt
---

# Good tests

# What makes good tests

- Tests should prevent regressions, and encourage correct code
- Tests shouldn't need to be changed unless requirements change
- Tests should be easy to change when requirements change
- It should be easy to change tests in isolation
- Tests should be easy to read

# What makes bad tests

- Tests can fail when they shouldn't
- Tests can succeed when they shouldn't
<!-- orthogonally -->
- Overtesting <!-- tests testing more things than they should -->
- Undertesting <!-- tests testing less things than they should -->
- Accidentally breaking tests, especially invisibly
- Unreadable tests
- Verbose tests

# TDD

Writing tests before fixing code

Tests *used to fail* before the code changed, *start to succeed* after the code's changed

Avoids undertesting

Orthogonal to what I'm going to suggest

# Matcher DSLs

Nice ways to write assertions

Cool error messages help to debug test failures

Examples

```hs
diff x (<) y
```

```scala
x shouldBe <(y)
```

Takes way too much effort to write custom ones

Replaced by what I'm going to suggest

# Table-driven testing

Test for a function `f :: a -> b` is a table of `(a, b)` pairs

Concise

Easy to understand

Easy to accumulate a bunch of tests and prevent regressions

Extremely easy to overtest

Hard to connect reqirements to test cases

Orthogonal to what I'm going to suggest

# Abstraction-negativity

Destroying all abstractions in tests

No utility functions

Decreased brittleness

Readability, nothing is hiding under the code

Updating tests is very hard

Writing tests is full of copypaste

Hard to read huge tests

# Abstraction-positivity

Utility functions and abstractions all over

Concise

Easy to read

Short tests, easy to write tests

Modifying utilities can implicitly modify multiple tests <!-- making tests harder to write -->

Modifying tests requires modifying utilities <!-- the abstractions are paper-thin -->

Hard to figure out what a test is actually doing

# Shit abstraction

Abstraction-negativity is a reaction to shit abstraction

Abstraction-positivity has so many negatives because
huffman-coding your tests is barely an abstraction

# Good abstractions

<!-- for illustrative purposes, working directly on these types is not necessary -->

```haskell
type Pred a = a -> Bool

type PT a b = Pred a -> Pred b

type Test i o = (i, Pred o)

test :: (i -> o) -> Test i o -> Bool
test f (i, p) = p . f $ i
```

<!-- "in the end" this is interpreted to `(String, Bool)` -->

# Why these are good

Lots of predicate transformers

Most are very *short*

Compose in lots of nice ways

All tests look like table-driven tests

Table-driven tests are a special case
<!-- being equal to a value is a predicate -->

# Why is it as good as table-driven tests

In a table-driven test the structure of the output we expect exactly matches the structure of the assertion

<!-- because the code we write *is* just an expected output for us to test against -->

More general tests specify predicates `o -> Bool` which have *backwards* structure compared to writing an `o` value

<!-- writing a predicate can involve pattern matching and branching instead of calling constructors. in short, predicates are contravariant.  -->

`PT a b` is almost a CPS transform of `a -> b`

<!-- except the result type of the continuation is fixed to `Bool` -->

CPS *reinverts control*

# Example

```hs
function :: Map k v -> Either e (Map k (Maybe o))
```

I want to specify a test of `function` in terms of a `Map k (v, Pred o)`, using predicate transformers.

For this type of test:

- `function` must return a `Right`, or we fail
- if `k` is present in the input, it must be present in the output, or we fail
- all `Maybe o`'s must be `Just` values, or we fail

# Transformers

```hs
right :: PT a (Either e a)
right = either (const False)

just :: PT a (Maybe a)
just = maybe False
```

# More transformers

```hs
list :: [Pred a] -> Pred [a]
list (p:ps) (x:xs) = p x && list ps xs
list [] [] = True
list _ _ = False

map :: Map k (Pred o) -> Pred (Map k o)
map ps m =
  let ps' = toList ps
      m'  = toList m in
  fst <$> ps' == fst <$> m' &&
  list (snd <$> ps') (snd <$> m')
```

# Back to the example

```hs
function :: Map k v -> Either e (Map k (Maybe o))

type FunctionTest
  = (Map k v, Pred (Either e (Map k (Maybe o))))

functionHappyPathTest
  :: Map k (v, Pred o)
  -> FunctionTest
functionHappyPathTest ps = (input, pred)
  where
  input = fst <$> ps
  pred = right $ map (just . snd <$> ps)
```

<!-- by now you should be convinced that predicate transformers let you write predicates in much the same way you'd write "expected output data". -->

# Appendix

# Making predicates look like data

```hs
type Shape f = f ()
shapeOf :: Functor f => f a -> Shape f
shapeOf fa = () <$ fa
distF
    :: (Functor f, Foldable f, Eq (Shape f))
    => f (Pred a) -> Pred (f a)
distF preds values =
    shapeOf preds == shapeOf values &&
    list (toList preds) (toList values)
```

# distF example

```hs
data BinTree a = Leaf a | Node (BinTree a) (BinTree a)
tree = Node (Leaf 1) (Node (Leaf 2) (Leaf 3))
test = Node (Leaf (< 10)) (Node (Leaf isEven) (Leaf isOdd))
distF test tree -- True
```

<!-- distF also subsumes our earlier `map` combinator -->

# Examples of predicate transformers

Function composition is a valid predicate transformer

```hs
pt :: (b -> a) -> (a -> c) -> b -> c
pt f p = p . f
```

# Examples of predicate transformers

```hs
all :: Foldable f => (a -> Bool) -> f a -> Bool
any :: Foldable f => (a -> Bool) -> f a -> Bool
```

==>

```hs
all :: Foldable f => PT a (f a)
any :: Foldable f => PT a (f a)
```

# Examples of predicate transformers

```hs
every :: Foldable f => f (Pred a) -> Pred a
every ps a = all ($ a) ps
```

# Examples of predicate transformers

```hs
match :: APrism s t a b -> PT a s
match p pred s =
  right pred $ matching p s
```

# Examples of predicate transformers

```hs
-- provided in lens
anyOf :: Getting Any s a -> PT a s
allOf :: Getting All s a -> PT a s
noneOf :: Getting Any s a -> PT a s
```
<!-- can be applied to a traversal, fold or getter -->

# Examples of predicate transformers

```hs
-- provided in lens
anyOf :: Getting Any s a -> PT a s
allOf :: Getting All s a -> PT a s
noneOf :: Getting Any s a -> PT a s
-- provided in predicate-transformers
allOf1 :: Getting (All, Any) s a -> PT a s
```

# Algebra of tests

- `Pred` is known to have `Contravariant`, `Decidable` and `Divisible` instances
<!-- in a newtype wrapper of course -->
- `PT` has a `Category` instance, ordinary `.` is its composition and `id` is its identity
<!-- like optics from the lens package -->
- `PT` can be generalized to `type FT c a b = (a -> c) -> b -> c`

# Generalized predicates

- For more reuse we can define predicates that abstract over their result type
```hs
class Predicatory a where
  otherHand :: HasCallStack => a -> a -> a
  also :: HasCallStack => a -> a -> a
  stop :: HasCallStack => a
  continue :: a
```
<!-- all we need is and, or, true, and false -->
- We can have predicates that return `IO ()`, throwing exceptions on predicate failure
<!-- this allows easy integration with testing frameworks as they currently exist, and easily running predicates in sequence -->

# Generalized predicates 2
- We can have predicates with extra parameters
```hs
instance Predicatory a => Predicatory (e -> a) where
  (f `otherHand` f') e = f e `otherHand` f' e
  (f `also` f') e = f e `also` f' e
  stop _ = stop
  continue _ = continue
```
<!-- this allows using `and` and `or` on predicates themselves, as well as making predicate transformers apply to functions that are not saturated enough to be "real" predicates yet -->
