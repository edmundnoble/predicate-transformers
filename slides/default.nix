# Credit for this excellent default.nix to https://qiita.com/kimagure/items/9d27015e12d4f22b53db, think it's by Justin Woo
let
  pkgs = import <nixpkgs> {};

  texlive = pkgs.texlive.combine {
    inherit (pkgs.texlive)
    scheme-medium
    noto
    mweights
    cm-super
    cmbright
    hfbright
    fontaxes
    framed
    fvextra
    minted
    upquote
    ifplatform
    xstring
    beamer;
  };
in {
  slides = pkgs.stdenv.mkDerivation {
    name = "slides";
    src = ./.;

    buildInputs = [
      texlive
      pkgs.pandoc
      pkgs.pythonPackages.pygments
      pkgs.watchexec
    ];
  };
}
