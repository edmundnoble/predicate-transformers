# Revision history for property-matchers

## 0.4
* Fix `allOf1` which would fail on a non-empty input, instead of the opposite
* Add `anyOf`
* Rename `allTrue` to `checkAll`


## 0.3
* Add `fails` function, for asserting that an exception is thrown
* Replace `propful` with `alignExact`
* Add `alignLax` generalizing `alignExact`
* Add `elem`
* Add fixity for `:=>` to be compatible with `?`
* Add comparisons other than `equals`


## 0.2

* Add `bool` function
* Add hspec + smallcheck test suite
* Add `branch` for non-backtracking disjunction of properties
* Monomorphize `fail`, `succeed`, `assess`, `branch` for ease of use in hspec
* Rename `PropertyFailed` exception type to `PropertyException` to recognize the new case for `branch`
* Add `type Assertion = IO ()`
* Add a few pragmas
* Reorder `PropertyMatchers` module for intelligibility


## 0.1

* Rename to property-matchers
* Rename kth to atIndex
* Change error message formatting for predicate failure


## predicate-transformers 0.17.0.0 -- 2024-10-13
* Rename multiple functions to shorter names with the intent that predicate-transformers be imported qualified.
  In particular `also` renamed to `and`, `otherHand` renamed to `or`, etc.
* Delete `lens` and `mtl` dependencies.
* Delete `soleElementOf`, for lack of use in favor of `match`.
* Rename `Predicatory` to `Boolish` for ease of reading.
* Delete `Exceptional` class, folding it into the newly renamed `Boolish`.
* Delete `predJust`, `predLeft`, and `predRight` in favor of `match` uses.


## predicate-transformers 0.16.0.0 -- 2024-10-13
* Add predicate failure error messages that include the actual value under test and a message explaining what was expected.
* Rename `just`, `left`, and `right` to `predJust`, `predLeft`, `predRight`


## predicate-transformers 0.15.0.0 -- 2024-08-23
* Change `?` to right associative so that it works with `.`.


## predicate-transformers 0.14.0.0 -- 2024-08-23
* Add `?` back in as a useful operator to work with `&`; `&` can be used as a predicate applicator, and `?` can be used as a predicate transformer applicator, allowing for a form of infix binary application like `x & f ? y = f x y`.
* Add `match` back in as a type-restricted alias for `soleElementOf`.


## predicate-transformers 0.13.0.0 -- 2024-07-23
* Remove `?`


## predicate-transformers 0.12.0.0 -- 2024-07-23
* Rename `sole` to `soleElement`
* Add `?`, an infix function application operator with lower
  precedence than `!`.
* Add `satAll = foldr also continue`.


## predicate-transformers 0.11.0.0 -- 2024-07-21
* Add documentation.
* Rename `onlyContains` to `sole`. Add `soleOf`, generalizing over `Fold`s.
  `soleOf` is likely a better replacement for `match` than `allOf1` was.
* Exchange `INLINABLE` pragmas for `-fexpose-all-unfoldings`.
* Implement `traceFailFunShow`.
* Change `Exceptional`'s method `assess` to make it possible to implement for functional predicates, and delete `traceFailFun`, now redundant.


## predicate-transformers 0.10.0.0 -- 2024-07-21
* Rename `oneOfTwo` to `otherHand`, for easier reading.
* Set `also` and `otherHand` precedences to those of `&&` and `||`
  respectively. That makes them work better with the precedence of
  `!`, allowing easier composition.
* Rename `only` to `onlyContains`, for easier reading and to avoid a
  name conflict with `Control.Lens.only`.
* Set `!` precedence to be equal to that of `.`.
* Add `equals` predicate.
* Delete `match`. `allOf1` does the same thing more generally, with
  `Fold` instead of `Prism`.
* Make `otherHand` stop catching async exceptions. Otherwise a thread
  being killed may appear as a predicate failure.


## predicate-transformers 0.9.0.0 -- 2024-07-21
* Add instance `Predicatory (e -> a)`. This will allow for adding extra
  parameters to predicates, making it easier to compose them; maybe
  these are called "functional predicates".
* Added `traceFailFun`. This version of traceFail works on functional
  predicates.
* Tupling sugar renamed from `==>` to `:=>`, to allow it to be a pattern synonym.
* Minor code style changes.


## predicate-transformers 0.1.0.0 -- 2019-10-05
* First version. Released on an unsuspecting world.
